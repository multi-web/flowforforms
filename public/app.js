import { state } from "./scripts/state.js";
import {initForm, navigateForm} from './scripts/actions.js'

const init = () => {
    initForm(state);
    navigateForm(state);
};

init();
