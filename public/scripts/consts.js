const CONTAINER = document.querySelector('[data-container]');
const INPUT_CONTAINER_LIST = document.querySelectorAll('[data-input-container]');
const INPUT_LIST = document.querySelectorAll('[data-input]');
const MODAL = document.querySelector('[data-modal]');
const MODAL_CONTAINER = document.querySelector('[data-modal-board]');
const TITLE = document.querySelector('[data-title]');
const FORM_CONTAINER = document.querySelector('[data-form-container]');
const BUTTON_SUBMIT = document.querySelector('[data-button-submit]');
const BUTTON_ENTRY = document.querySelector('[data-button-entry]');
const BUTTON_CLEAR = document.querySelector('[data-button-clear]');

export {
    CONTAINER,
    INPUT_CONTAINER_LIST,
    INPUT_LIST,
    MODAL,
    MODAL_CONTAINER,
    TITLE,
    FORM_CONTAINER,
    BUTTON_SUBMIT,
    BUTTON_ENTRY,
    BUTTON_CLEAR,
};
