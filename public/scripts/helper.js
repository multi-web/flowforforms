const getElementPosition = element => {
    return {
        'x': element.getBoundingClientRect().left,
        'y': element.getBoundingClientRect().top,
        'width': element.offsetWidth
    }
};

const moveElement = (elementToGetPosition, element) => {
    let x = getElementPosition(elementToGetPosition).x;
    let y = getElementPosition(elementToGetPosition).y;
    let width = getElementPosition(elementToGetPosition).width;
    element.style.position = 'absolute';
    element.style.left = x + "px";
    element.style.top = y + "px";
    element.style.width = width + "px";
};

const removeAllChildren = element => {
    while (element.hasChildNodes()) {
        element.removeChild(element.lastChild);
    }
};

const createModalContent = (toClone, modalContainer) => {
    let elementToClone = toClone.cloneNode(true);
    removeAllChildren(modalContainer);
    modalContainer.appendChild(elementToClone);
    let newInput = modalContainer.querySelector('input');
    newInput.focus();
    newInput.selectionStart = newInput.selectionEnd = newInput.value.length;
};

const displayValidationText = (input, textContainer) => {
    input.style.borderColor = "#ff3860";
    textContainer.style.display = 'block';
    textContainer.classList.add('bounce');
};

const isValidate = value => {
    return value !== '';
};

const setCurrentPosition = (currentPos, direction, inputList, modal) => {
    if (direction === 'ArrowUp') {
        return currentPos >= 1 ? currentPos - 1 : currentPos
    }
    if (direction === 'ArrowDown') {
        if (currentPos < inputList.length - 1) return currentPos + 1;
        else {
            modal.style.display = 'none';
            return currentPos
        }
    }
};

const clearForm = (inputList, state) => {
    inputList.forEach(input => input.querySelector('input').value = '');
    state.currentPosition = 0;
};

const entry = (modal, form, button, state, inputList, modalContainer) => {
    button.hidden = true;
    form.style.display = 'block';
    modal.style.display = 'block';
    createModalContent(inputList[state.currentPosition], modalContainer);
    moveElement(inputList[state.currentPosition], modalContainer.querySelector('[data-input-container]'));
};

const submit = (formContainer, titleContainer, container) => {
    formContainer.style.display = 'none';
    titleContainer.style.display = 'block';
    titleContainer.classList.add('fadeIn');
    container.style.backgroundImage = 'url("Road2.jpg")'
};

const navigate = (event, inputList, modalContainer, state, modal) => {
    if (event.key === 'ArrowUp' || event.key === 'ArrowDown') {
        let currentInput = inputList[state.currentPosition].querySelector('input');
        let modalInput = modalContainer.querySelector('input');
        let modalValidText = modalContainer.querySelector('[data-validation-message]');
        event.preventDefault();
        if(isValidate(modalInput.value)) {
            currentInput.value = modalInput.value;
            state.currentPosition = setCurrentPosition(state.currentPosition, event.key, inputList, modal);
            createModalContent(inputList[state.currentPosition], modalContainer);
            moveElement(inputList[state.currentPosition], modalContainer.querySelector('[data-input-container]'));
        } else {
            displayValidationText(modalInput,modalValidText);
        }
    }
};

export {entry, navigate, clearForm, submit}
