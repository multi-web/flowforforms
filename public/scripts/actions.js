import {entry, submit, navigate, clearForm} from "./helper.js";
import {
    CONTAINER,
    INPUT_CONTAINER_LIST,
    INPUT_LIST,
    MODAL,
    MODAL_CONTAINER,
    TITLE,
    FORM_CONTAINER,
    BUTTON_SUBMIT,
    BUTTON_ENTRY,
    BUTTON_CLEAR,
} from './consts.js'

const initForm = state => {
    BUTTON_SUBMIT.onclick = () => submit(FORM_CONTAINER, TITLE, CONTAINER);
    BUTTON_ENTRY.onclick = () => entry(MODAL, FORM_CONTAINER, BUTTON_ENTRY, state, INPUT_CONTAINER_LIST, MODAL_CONTAINER);
    BUTTON_CLEAR.onclick = () => {
        clearForm(INPUT_CONTAINER_LIST, state);
        entry(MODAL, FORM_CONTAINER, BUTTON_ENTRY, state, INPUT_CONTAINER_LIST, MODAL_CONTAINER);
    };
};

const navigateForm = state => {
    window.onkeydown = event => navigate(event, INPUT_CONTAINER_LIST, MODAL_CONTAINER, state, MODAL);
};

export {initForm, navigateForm}
